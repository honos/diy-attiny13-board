# DIY Jednoducha vyvojova deska pro ATTiny13

 **Jednotlive varianty jsou umisteny v jednotlivych branchich**

### Vlastnosti ATTiny13:
 - x6 IO (RESET [pin 1] se muze taky pouzit jako IO)
 - x4 ADC (10 bit)
 - x6 DIO
 - x1 externi preruseni na pinu D1 (PB1)
 - 1k bytu flash
 - 128 bytu SRAM
 - 64 bytu EEPROM
 - x1 SPI
 - x2 PWM na pinech D0 a D1 (PB0, PB1)
 - Takt az do 20MHz (externi XTAL)
 - Napajeni 1.8V - 5.5V
 - Zatez az 40mA na jeden pin
 - Zatez az 200 mA na vsechny piny soucasne!


### Vlastnosti desky
 - Napajeni 5V
 - Kondenzator pro vyrovnani rusiveho napeti
 - Programuje se pomoci Arduino ICSP
 - Led signalizujici napeti
 - Bez resetovaciho tlacitka (Pro reset je nutne odpojit napajeni)

Pro programovani pomoci arduino IDE je mozne (nutne?) pouzit [*core* od *SLEEMANJ*](https://github.com/sleemanj/optiboot/blob/master/dists/README.md) nebo [MicroCore od MCUDude](https://github.com/MCUdude/MicroCore)

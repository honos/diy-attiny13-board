EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AMS1117
LIBS:attiny13-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "DIY ATTiny13 Board"
Date "2018-06-13"
Rev "1"
Comp "Denis Colesnicov"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATTINY13A-PU U1
U 1 1 5B1FE5EC
P 4400 4400
F 0 "U1" H 3600 4800 50  0000 C CNN
F 1 "ATTINY13A-PU" H 5050 4000 50  0000 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 5050 4400 50  0001 C CIN
F 3 "" H 3600 4750 50  0001 C CNN
	1    4400 4400
	-1   0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5B1FE62C
P 3050 4400
F 0 "C1" H 3075 4500 50  0000 L CNN
F 1 "C" H 3075 4300 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3088 4250 50  0001 C CNN
F 3 "" H 3050 4400 50  0001 C CNN
	1    3050 4400
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x03_Odd_Even J3
U 1 1 5B1FE6D5
P 10050 2150
F 0 "J3" H 10100 2350 50  0000 C CNN
F 1 "ICSP" H 10100 1950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 10050 2150 50  0001 C CNN
F 3 "" H 10050 2150 50  0001 C CNN
	1    10050 2150
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5B1FE703
P 1150 7150
F 0 "R2" V 1230 7150 50  0000 C CNN
F 1 "10k" V 1150 7150 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1080 7150 50  0001 C CNN
F 3 "" H 1150 7150 50  0001 C CNN
	1    1150 7150
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR01
U 1 1 5B1FE738
P 3050 4150
F 0 "#PWR01" H 3050 4000 50  0001 C CNN
F 1 "VCC" H 3050 4300 50  0000 C CNN
F 2 "" H 3050 4150 50  0001 C CNN
F 3 "" H 3050 4150 50  0001 C CNN
	1    3050 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5B1FE848
P 3050 4650
F 0 "#PWR02" H 3050 4400 50  0001 C CNN
F 1 "GND" H 3050 4500 50  0000 C CNN
F 2 "" H 3050 4650 50  0001 C CNN
F 3 "" H 3050 4650 50  0001 C CNN
	1    3050 4650
	1    0    0    -1  
$EndComp
Text GLabel 9850 2050 0    60   Input ~ 0
MISO
Text GLabel 9850 2150 0    60   Input ~ 0
SCK
Text GLabel 9850 2250 0    60   Input ~ 0
RESET
Text GLabel 10350 2050 2    60   Input ~ 0
+5V
Text GLabel 10350 2150 2    60   Input ~ 0
MOSI
Text GLabel 10350 2250 2    60   Input ~ 0
GND
Text GLabel 5750 7050 2    60   Input ~ 0
+5V
Text GLabel 5750 7250 2    60   Input ~ 0
GND
$Comp
L VCC #PWR03
U 1 1 5B1FE972
P 5700 6950
F 0 "#PWR03" H 5700 6800 50  0001 C CNN
F 1 "VCC" H 5700 7100 50  0000 C CNN
F 2 "" H 5700 6950 50  0001 C CNN
F 3 "" H 5700 6950 50  0001 C CNN
	1    5700 6950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5B1FE989
P 5700 7300
F 0 "#PWR04" H 5700 7050 50  0001 C CNN
F 1 "GND" H 5700 7150 50  0000 C CNN
F 2 "" H 5700 7300 50  0001 C CNN
F 3 "" H 5700 7300 50  0001 C CNN
	1    5700 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 7050 5700 7050
Wire Wire Line
	5700 7050 5700 6950
Wire Wire Line
	5750 7250 5700 7250
Wire Wire Line
	5700 7250 5700 7300
Wire Wire Line
	3050 4250 3050 4150
Text GLabel 6450 4150 3    60   Input ~ 0
MOSI
Text GLabel 5750 4350 3    60   Input ~ 0
SCK
Text GLabel 6100 4250 3    60   Input ~ 0
MISO
Text GLabel 5400 4950 3    60   Input ~ 0
RESET
Text GLabel 1150 7000 1    60   Input ~ 0
+5V
Text GLabel 1150 7300 3    60   Input ~ 0
RESET
$Comp
L Conn_01x05 J2
U 1 1 5B1FEF30
P 10950 6000
F 0 "J2" H 10950 6300 50  0000 C CNN
F 1 "right" H 10950 5700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 10950 6000 50  0001 C CNN
F 3 "" H 10950 6000 50  0001 C CNN
	1    10950 6000
	1    0    0    -1  
$EndComp
Text GLabel 6550 4150 2    60   Input ~ 0
5
Text GLabel 6200 4250 2    60   Input ~ 0
6
Text GLabel 5850 4350 2    60   Input ~ 0
7
Text GLabel 5400 4450 2    60   Input ~ 0
2
Text GLabel 5400 4550 2    60   Input ~ 0
3
Text GLabel 5400 4650 2    60   Input ~ 0
1
Text GLabel 10750 6200 0    60   Input ~ 0
5
Text GLabel 10750 6100 0    60   Input ~ 0
6
Text GLabel 10750 6000 0    60   Input ~ 0
7
Text GLabel 10750 5900 0    60   Input ~ 0
+5V
Text GLabel 10750 5800 0    60   Input ~ 0
GND
$Comp
L LED D1
U 1 1 5B1FF753
P 9800 3700
F 0 "D1" H 9800 3800 50  0000 C CNN
F 1 "Power" H 9800 3600 50  0000 C CNN
F 2 "LEDs:LED_1206_HandSoldering" H 9800 3700 50  0001 C CNN
F 3 "" H 9800 3700 50  0001 C CNN
	1    9800 3700
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B1FF759
P 10100 3700
F 0 "R1" V 10180 3700 50  0000 C CNN
F 1 "R" V 10100 3700 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 10030 3700 50  0001 C CNN
F 3 "" H 10100 3700 50  0001 C CNN
	1    10100 3700
	0    1    1    0   
$EndComp
Text GLabel 9650 3700 0    60   Input ~ 0
GND
Text GLabel 10250 3700 2    60   Input ~ 0
+5V
Text GLabel 9650 6000 2    60   Input ~ 0
1
Text GLabel 9650 6100 2    60   Input ~ 0
2
Text GLabel 9650 6200 2    60   Input ~ 0
3
Connection ~ 5700 6950
Connection ~ 5700 7050
Connection ~ 5700 7250
Wire Wire Line
	3400 4650 3050 4650
Wire Wire Line
	3050 4650 3050 4550
Wire Wire Line
	3050 4150 3400 4150
Wire Wire Line
	5400 4950 5400 4650
Wire Wire Line
	5400 4350 5850 4350
Wire Wire Line
	5400 4250 6200 4250
Wire Wire Line
	5400 4150 6550 4150
$Comp
L Conn_01x05 J1
U 1 1 5B20C4CA
P 9450 6000
F 0 "J1" H 9450 6300 50  0000 C CNN
F 1 "left" H 9450 5700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 9450 6000 50  0001 C CNN
F 3 "" H 9450 6000 50  0001 C CNN
	1    9450 6000
	-1   0    0    -1  
$EndComp
Text GLabel 9650 5900 2    60   Input ~ 0
+5V
Text GLabel 9650 5800 2    60   Input ~ 0
GND
Wire Notes Line
	8800 500  8800 6550
Wire Notes Line
	8800 2900 11200 2900
Wire Notes Line
	8800 4700 11200 4700
Text Notes 9400 800  0    60   ~ 0
ICSP/SPI/POWER\n         port
Text Notes 9700 3150 0    60   ~ 0
Power LED
Text Notes 9900 4950 0    60   ~ 0
IO ports
Wire Notes Line
	6950 6550 500  6550
Wire Notes Line
	3650 6550 3650 7800
$EndSCHEMATC
